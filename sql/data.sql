LOAD DATA LOCAL INFILE
	-- 'D:/Java/Eclipse/Spring/BeerMap/temp/data/countries.csv'
    'C:/Users/RENT/eclipse-workspace/Spring/BeerMap/temp/data/countries.csv'
    INTO TABLE `beermap`.`country`
    COLUMNS TERMINATED BY ';'
    LINES TERMINATED BY '\r\n';
    
INSERT INTO `user` (`username`, `password`, `email`, `dateOfBirth`, `country_id`) VALUES
	('Rambo', 'rambo', 'rambo@gmail.com', '1960-01-01', 192),
	('Balboa', 'balboa', 'balboa@gmail.com', '1960-01-02', 245),
	('Frodo', 'frodo', 'frodo@gmail.com', '1981-01-28', 162),
	('Aragorn', 'aragorn', 'aragorn@gmail.com', '1958-10-20', 192),
	('Gandalf', 'gandalf', 'gandalf@gmail.com', '1939-05-25', 144),
	('Arwena', 'arwena', 'arwena@gmail.com', '1977-07-01', 162),
	('Legolas', 'legolas', 'legolas@gmail.com', '1977-01-13', 192),
	('Gimli', 'gimli', 'gimli@gmail.com', '1944-05-05', 143),
	('Golum', 'golum', 'golum@gmail.com', '1899-12-31', 138),
	('Vader', 'vader', 'vader@gmail.com', '1940-01-01', 143);

INSERT INTO `family` (`name`) VALUES
	('JASNY LAGER'),
	('PILSNER'),
	('CZERWONY LAGER'),
	('CIEMNY LAGER'),
	('KOŹLAK'),
	('JASNE ALE'),
	('IPA'),
	('CZERWONE ALE'),
	('BRĄZOWE ALE'),
	('PORTER'),
	('STOUT'),
	('MOCNE ALE'),
	('PIWO PSZENICZNE'),
	('KWAŚNE ALE'),
	('PIWO SPECJALNE');
	
LOAD DATA LOCAL INFILE
	-- 'D:/Java/Eclipse/Spring/BeerMap/temp/data/style.csv'
    'C:/Users/RENT/eclipse-workspace/Spring/BeerMap/temp/data/style.csv'
    INTO TABLE `beermap`.`style`
    COLUMNS TERMINATED BY ';'
    LINES TERMINATED BY '\r\n';

INSERT INTO `beer` (`name`, `alcohol`, `extract`, `style_id`, `country_id`) VALUES
	('Birbant Kiss The Beast', 19.1, 8.5, 46, 162),
	('Hopkins Black Jack', 21.0, 9.0, 63, 162),
	('Pinta Atak Chmielu', 15.1, 6.1, 39, 162),
	('Pinta Brett IPA', 15.1, 7.5, 101, 162),
	('Pinta Hopus Pokus', 19.1, 8.5, 41, 162),
	('Amber Chmielowy', 11.3, 5.0, 11, 162),
	('Amber Naturalny', 12.2, 5.7, 3, 162),
	('Amber Johaness', 14.5, 6.5, 3, 162),
	('Lwówek Jankes', 12.5, 4.2, 33, 162),
	('Guinness Draught', 9.8, 4.2, 67, 87);

