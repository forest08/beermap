DROP SCHEMA IF EXISTS `beermap`;
CREATE SCHEMA `beermap` CHAR SET `utf8` COLLATE `utf8_polish_ci`;
USE `beermap`;

CREATE TABLE `country` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	CONSTRAINT `country_pk` PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(128) NOT NULL,
	`password` VARCHAR(128) NOT NULL,
	`email` VARCHAR(128) NOT NULL,
	`dateOfBirth` DATE NOT NULL,
    `country_id` INT,
	CONSTRAINT `user_pk` PRIMARY KEY (`id`),
    CONSTRAINT `user_country_fk` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
);

CREATE TABLE `family` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	CONSTRAINT `family_pk` PRIMARY KEY (`id`)
);

CREATE TABLE `style` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	`nameBJCP` VARCHAR(128),
	`categoryBJCP` VARCHAR(8),
    `family_id` INT,
	CONSTRAINT `style_pk` PRIMARY KEY (`id`),
    CONSTRAINT `style_family_fk` FOREIGN KEY (`family_id`) REFERENCES `family` (`id`)
);

CREATE TABLE `beer` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
    `alcohol` DOUBLE(3,1),
    `extract` DOUBLE(3,1),    
    `style_id` INT,
    `country_id` INT,
    `avgRate` DOUBLE(4,2),
	CONSTRAINT `beer_pk` PRIMARY KEY (`id`),
    CONSTRAINT `beer_style_fk` FOREIGN KEY (`style_id`) REFERENCES `style` (`id`),
    CONSTRAINT `beer_country_fk` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
);

CREATE TABLE `rate` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`aroma` INT NOT NULL,
    `appearance` INT NOT NULL,
    `taste` INT NOT NULL,
    `overall` INT NOT NULL,
    `total` INT AS(`aroma` + `appearance` + `taste` + `overall`),
    `comment` VARCHAR(1024),
    `user_id` INT,
    `beer_id` INT,
	CONSTRAINT `rate_pk` PRIMARY KEY (`id`),
    CONSTRAINT `rate_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
    CONSTRAINT `rate_beer_fk` FOREIGN KEY (`beer_id`) REFERENCES `beer` (`id`)
);