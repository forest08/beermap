﻿package sda.beermap.config;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Do konfiguracji serwletu dystrybutora i kontekstu aplikacji Springa w kontekście serwletu
 * wystarczy dowolna klasa rozszerzająca AbstractAnnotationConfigDispatcherServletInitializer
 * (i w ten sposób implementująca interfejs WebApplicationInitializer).
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected String[] getServletMappings() {
		// Odwzorowujemy serwlet dystrybutora na ścieżkę "/" (domyślny serwlet aplikacji).
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// Zwrócone klasy definiują komponenty dla kontekstu aplikacji serwletu nasłuchującego (ContextLoaderListener).
		return new Class<?>[] { RootConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// Zwrócone klasy konfigurują kontekst aplikacji dla serwletu dystrybutora (DispatcherServlet).
		return new Class<?>[] { WebConfig.class };
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);
		return new Filter[] { characterEncodingFilter };
	}
}