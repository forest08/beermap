﻿package sda.beermap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Konfiguracja Spring MVC
 */
@Configuration
@EnableWebMvc 								// Włączamy Spring MVC
@ComponentScan("sda.beermap.controller") 	// Włączamy skanowanie komponentów
public class WebConfig implements WebMvcConfigurer {

	/*
	 * Konfigurujemy producenta widoków JSP. Producent widoku służy do wyszukiwania
	 * plików JSP poprzez dodawanie prefiksu i sufiksu do nazw widoków (na przykład
	 * widok o nazwie home zostanie odczytany jako /WEB-INF/views/home.jsp).
	 */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/view/");
		resolver.setSuffix(".jsp");
		resolver.setExposeContextBeansAsAttributes(true);
		return resolver;
	}

	/*
	 * Konfigurujemy obsługę statycznych zasobów.
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	// @Override
	// public void addResourceHandlers(ResourceHandlerRegistry registry) {
	// registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	// }

	// @Override
	// public void addViewControllers(ViewControllerRegistry registry) {
	// registry.addViewController("/").setViewName("home");
	// }

}