package sda.beermap.dao;

import org.springframework.data.repository.CrudRepository;

import sda.beermap.model.Beer;

public interface BeerRepository extends CrudRepository<Beer, Integer> {
	
	
}
