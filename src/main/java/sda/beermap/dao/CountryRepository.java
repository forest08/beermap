package sda.beermap.dao;

import org.springframework.data.repository.CrudRepository;

import sda.beermap.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {

}
