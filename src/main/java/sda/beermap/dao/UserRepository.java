package sda.beermap.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sda.beermap.model.User;
import java.lang.String;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

//	@Query("select u from User u where u.username = :username and u.password = :password")
//	User login(
//			@Param("username") String username,
//			@Param("password") String password);

	List<User> findByUsername(String username);

}

