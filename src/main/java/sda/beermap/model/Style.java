package sda.beermap.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Entity
public class Style {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotEmpty
	private String name;
	
	private String nameBJCP;
	
	private String categoryBJCP;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="family_id")
	private Family family;

	public Style() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameBJCP() {
		return nameBJCP;
	}

	public void setNameBJCP(String nameBJCP) {
		this.nameBJCP = nameBJCP;
	}

	public String getCategoryBJCP() {
		return categoryBJCP;
	}

	public void setCategoryBJCP(String categoryBJCP) {
		this.categoryBJCP = categoryBJCP;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
