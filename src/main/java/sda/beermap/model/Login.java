package sda.beermap.model;

import javax.validation.constraints.Size;

public class Login {
	
	@Size(min = 3, max = 25, message = "{username.size}")
	private String username;
	
	@Size(min = 3, max = 25, message = "{password.size}")
	private String password;

	public Login() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
