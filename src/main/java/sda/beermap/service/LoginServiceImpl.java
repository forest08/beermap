package sda.beermap.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sda.beermap.dao.UserRepository;
import sda.beermap.model.Login;
import sda.beermap.model.User;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User register(User user) {
		return userRepository.save(user);
	}

	@Override
	public boolean validateUser(Login login) {
		List<User> users = userRepository.findByUsername(login.getUsername());
		if(users.size() != 1){
			return false;
		}
		if(users.get(0).getPassword().equals(login.getPassword())){
			return true;
		}
		return false;
	}

}
