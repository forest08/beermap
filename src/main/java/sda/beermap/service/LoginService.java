package sda.beermap.service;

import sda.beermap.model.Login;
//import sda.beermap.model.Login;
import sda.beermap.model.User;

public interface LoginService {
	
	User register(User user);
	
	boolean validateUser(Login login);
}
