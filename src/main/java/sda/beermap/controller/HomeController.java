﻿package sda.beermap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * W Spring MVC kontrolery są klasami zawierającymi metody oznaczone adnotacją
 * @RequestMapping do określenia rodzajów żądań, które chcemy obsłużyć.
 */
@Controller 						// Deklarujemy, że klasa ma być kontrolerem
@RequestMapping({ "/", "/home" }) 	// Odwzorowujemy kontroler na zasoby / i /home
public class HomeController {

	/*
	 * Obsługujemy żądania typu GET.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		// Nazwą widoku jest index. Serwlet dystrybutora zwróci się do producenta widoków
		// z prośbą o odwzorowanie tej nazwy na rzeczywisty widok	
		// (nazwa widoku "index" zostanie rozwiązana jako plik JSP /WEB-INF/views/index.jsp.
		return "index";
	}

}
