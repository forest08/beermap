package sda.beermap.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/feedback")
public class FeedbackController {

	@RequestMapping(method = RequestMethod.GET)
	public String feedback() {
		return "feedback";
	}
}
