package sda.beermap.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sda.beermap.dao.CountryRepository;
import sda.beermap.model.Country;
import sda.beermap.model.User;
import sda.beermap.service.LoginService;

@Controller
public class RegistrationController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private CountryRepository countryRepository;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegistrationForm(Model model) {
		model.addAttribute(new User());
		return "register";
	}

	@RequestMapping(value = "/register.do", method = RequestMethod.POST)
	public String processRegistration(@Valid User user, Errors errors) {
		if (errors.hasErrors()) {
			return "register";
		}
		loginService.register(user);
		return "redirect:/users/" + user.getUsername();
	}

	@ModelAttribute("countryList")
	public Iterable<Country> getCountryList() {
		return countryRepository.findAll();
	}

}
