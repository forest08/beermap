package sda.beermap.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sda.beermap.dao.UserRepository;
import sda.beermap.model.User;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	// private UserService userService;
	//
	// @Autowired
	// public void setUserService(UserService userService) {
	// this.userService = userService;
	// }

	@RequestMapping(method = RequestMethod.GET)
	public String showUsers(ModelMap model) {
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "users";
	}

//	@RequestMapping(value = "/{username}", method = RequestMethod.GET)
//	public String showUserProfile(@PathVariable String username, Model model) {
//		User user = userRepository.findByUsername(username).get(0);
//		model.addAttribute(user);
//		return "user";
//	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showUserProfileById(@PathVariable("id") int userId, Model model) {
		Optional<User> user = userRepository.findById(userId);
		model.addAttribute("user", user.get());
		return "user";
	}

}
