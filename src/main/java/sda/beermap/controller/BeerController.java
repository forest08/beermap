package sda.beermap.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sda.beermap.dao.BeerRepository;
import sda.beermap.model.Beer;
import sda.beermap.model.User;

@Controller
@RequestMapping("/beers")
public class BeerController {
	
	@Autowired
	private BeerRepository beerRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public String showBeers(ModelMap model) {
		List<Beer> listOfBeers = (List<Beer>) beerRepository.findAll();
		model.addAttribute("beers", listOfBeers);
		return "beers";
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String showUserProfile(@PathVariable("id") int beerId, Model model) {
		Optional<Beer> beer = beerRepository.findById(beerId);
		model.addAttribute("beer", beer.get());
		return "beer";
	}
	
}
