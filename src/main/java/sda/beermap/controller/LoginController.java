package sda.beermap.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sda.beermap.dao.UserRepository;
import sda.beermap.model.Login;
import sda.beermap.model.User;
import sda.beermap.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginForm(Model model) {
		model.addAttribute(new Login());
		return "login";
	}

	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	public String processLogin(HttpSession session, Model model,
//							   @ModelAttribute("login") Login login) {
							   @Valid Login login, Errors errors) {
		boolean loginSucessful = loginService.validateUser(login);
		if (loginSucessful) {
			session.setAttribute("username", login.getUsername());
//			User user = userRepository.findByUsername(login.getUsername()).get(0);
//			model.addAttribute("user", user);
			return "profile";
		} else {
			model.addAttribute("message", "Niepoprawne hasło lub nazwa użytkownika!");
			return "login";
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logoutPOST(HttpServletRequest request) {
		request.getSession().invalidate();
		return "index";
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "index";
	}
//	@RequestMapping("/logout")
//	public String logout(HttpSession session) {
//		session.invalidate();
//		return "index";
//	}

}
