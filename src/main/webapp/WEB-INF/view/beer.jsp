<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Piwo</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    <br /><br /><br />

    <h1>Piwo ${beer.name}</h1>
    <table class="beers">
		<tr>
			<td>Nazwa</td>
			<td><b>${beer.name}</b></td>
		</tr>
		<tr>
			<td>Alkohol (% obj.)</td>
			<td><b>${beer.alcohol}</b></td>
		</tr>
		<tr>
			<td>Ekstrakt (% wag.)</td>
			<td><b>${beer.extract}</b></td>
		</tr>
		<tr>
			<td>Kraj</td>
			<td><b>${beer.country}</b></td>
		</tr>
		<tr>
			<td>Styl</td>
			<td><b>${beer.style.name}</b></td>
		</tr>
		<tr>
			<td>Rodzina</td>
			<td><b>${beer.style.family.name}</b></td>
		</tr>
<!-- 		<tr> -->
<!-- 			<td>Średnia ocena</td> -->
<%-- 			<td><b>${beer.avgRate}</b></td> --%>
<!-- 		</tr> -->
	</table>
	
	<%@include file="../jspf/footer.jspf"%>
</body>
</html>