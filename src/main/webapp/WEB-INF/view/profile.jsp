<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Profil</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    
    <h1>Welcome ${sessionScope.username}</h1>

    <h1>Twój profil</h1>
    <table>
		<tr>
			<td></td>
		</tr>
	</table>
	<c:out value="${user.username}"/><br/>
	<c:out value="${user.email}"/>
	<c:out value="${user.dateOfBirth}"/>
	<c:out value="${user.country}"/>
	
	<%@include file="../jspf/footer.jspf"%>
</body>
</html>