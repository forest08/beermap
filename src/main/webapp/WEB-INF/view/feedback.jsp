<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
	<title>Feedback</title>
	<meta charset="UTF-8"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    <br /><br /><br /><hr />
    
	<div id="feedback">	
        <h1>Formularz kontaktowy</h1>      
        <form action="/feedback" method="POST" id="feedbackForm" name="feedbackForm">
            <br />
            <strong>Temat<br /></strong>
            <select class="feedback" name="subject">
                <option value="buisness">Reklama</option>
                <option value="correction">Błędne informacje na stronie</option>
                <option value="suggestion">Sugestie</option>           
                <option value="closeMyAccount">Usunięcie konta</option>
                <option value="other">Inne</option>
            </select>
            <br /><br />
            <strong>Wiadomość<br /></strong>
            <textarea class="feedback" rows=12 id="Body" name="Body" ></textarea><br /><br />
            <button type="button" class="sendbtn">Wyślij</button> 
        </form>
    </div>
    
    <%@include file="../jspf/footer.jspf"%>
</body>
</html>