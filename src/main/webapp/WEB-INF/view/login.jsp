<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pl">
<head>
	<title>Logowanie</title>
	<meta charset="UTF-8"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>

	<%@include file="../jspf/menu.jspf"%>
	<br /><br /><br />

	<f:form id="loginForm" action="login.do" modelAttribute="login" method="POST">
		<div class="imgContainer">
			<img src="/resources/images/img_avatar2.png" alt="Avatar" class="avatar">
		</div>

		<div class="container">
			<f:label path="username"><b>Login</b></f:label>
			<f:input path="username" placeholder="Wprowadź login" name="username"/>
			
			<f:label path="password"><b>Hasło</b></f:label>
			<f:password path="password" placeholder="Wprowadź hasło" name="password"/>	
			
			<f:button type="submit" class="login">Zaloguj</f:button>
			<input type="checkbox" checked="checked"/> Zapamiętaj mnie
			
			<f:errors path="*" element="div" cssClass="errors" />
			<c:if test = "${not empty message}">
         		<div class="errors"><c:out value="${message}"/></div>
      		</c:if>
		</div>

		<div class="container" style="background-color:#f1f1f1">
			<a href="/home"><button type="button" class="cancelbtn">Zrezygnuj</button></a>
			<span class="password"><a href="http://funnymemes.in/memes/Password_Incorrect_Funny_Meme.jpg">
				Zapomniałem hasła</a></span>
		</div>
	</f:form>
	
	<%@include file="../jspf/footer.jspf"%>
</body>
</html>