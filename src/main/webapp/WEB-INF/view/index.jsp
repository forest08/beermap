<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
	<title>BeerMap</title>
	<meta charset="UTF-8"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>

	<%@include file="../jspf/menu.jspf"%>
      
	<div>
    	<img src="/resources/images/home/home4.jpg"/>
    </div>
    
    <section class="best">
        <div class="carousel">
            <div class="bestItem">
                <h2>Wyróżnione piwa</h2>
            </div>
            <div class="bestItem">
                <a href="/beer/2" title="Zobacz oceny piwa">
                    <img class="bestItem" src="/resources/images/beer/pinta_hopus_pokus.jpg"
                    	alt="Pinta Hopus Pokus"><h3>Pinta Hopus Pokus</h3></a>
                <p><a href="/beer/top-10/">zobacz listę top 10 &raquo;</a></p>
            </div>
            <div class="bestItem">
                <a href="/beer/1" title="Zobacz oceny piwa">
                    <img class="bestItem" src="/resources/images/beer/birbant_kiss_the_beast.jpg"
                    alt="Bir  Kiss The Beast"><h3>Birbant Kiss The Beast</h3></a>
                <p><a href="/beer/top-pop/">zobacz najpopularniejsze &raquo;</a></p>
            </div>            
        </div>
	</section>
	
	<%@include file="../jspf/footer.jspf"%>
</body>
</html>