<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Użytkownik</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    <br /><br /><br />

    <h1>Użytkownik ${user.username}</h1>
    <table class="users">
		<tr>
			<td>Nazwa użytkownika</td>
			<td><b>${user.username}</b></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><b>${user.email}</b></td>
		</tr>
		<tr>
			<td>Data urodzenia</td>
			<td><b>${user.dateOfBirth}</b></td>
		</tr>
		<tr>
			<td>Kraj</td>
			<td><b>${user.country}</b></td>
		</tr>
		<tr>
			<td>Ocenione piwa</td>
			<td><b>${user.rates.size()}</b></td>
		</tr>
	</table>
	
	<%@include file="../jspf/footer.jspf"%>
</body>
</html>