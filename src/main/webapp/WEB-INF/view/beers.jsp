<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Katalog piw</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    <br /><br /><br /><hr />

    <c:choose>
        <c:when test="${beers.size() == 0}">
            <span style="font-size:16px; font-weight:bold;">Brak piw w katalogu!</span>
        </c:when>
        <c:otherwise>
            <table class="beers">
                <caption>KATALOG PIW</caption>
                <tr>
                    <th>ID</th>
                    <th>Nazwa piwa</th>
                    <th>Alk. (% obj.)</th>
                    <th>Ekstrakt (% wag.)</th>
<!--                     <th>Styl</th> -->
                </tr>
            <c:forEach items="${beers}" var="beer">
                <tr>
                    <td>${beer.id}</td>
                    <td><a href="beers/${beer.id}">${beer.name}</a></td>
                    <td>${beer.alcohol}</td>
                    <td>${beer.extract}</td>
<%--                     <td>${beer.style.name}</td> --%>
                </tr>
            </c:forEach>
            </table>
        </c:otherwise>
    </c:choose>
    
    <%@include file="../jspf/footer.jspf"%>
</body>
</html>