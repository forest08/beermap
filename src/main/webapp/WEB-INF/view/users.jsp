<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	isELIgnored="false" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Użytkownicy</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/style.css"/>
</head>
<body>
    <%@include file="../jspf/menu.jspf"%>
    <br /><br /><br /><hr />

    <c:choose>
        <c:when test="${users.size() == 0}">
            <span style="font-size:16px; font-weight:bold;">Brak użytkowników!</span>
        </c:when>
        <c:otherwise>
            <table class="users">
                <caption>UŻYTKOWNICY</caption>
                <tr>
                    <th>ID</th>
                    <th>Nazwa użytkownika</th>
                </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td><a href="users/${user.id}">${user.username}</a></td>
                </tr>
            </c:forEach>
            </table>
        </c:otherwise>
    </c:choose>
    
    <%@include file="../jspf/footer.jspf"%>
</body>
</html>