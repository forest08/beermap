﻿package sda.beermap.controller;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class HomeControllerTest {

    @Test
    public void testHomePage() throws Exception {
        HomeController controller = new HomeController();

        // Ustawiamy atrapę MockMvc
        MockMvc mockMvc = standaloneSetup(controller).build();
        
        // Wywołujemy żądanie GET o zasób "/" i oczekujemy zwrócenia widoku index
        mockMvc.perform(get("/")).andExpect(view().name("index"));
        mockMvc.perform(get("/home")).andExpect(view().name("index"));
    }

}