package sda.beermap.controller;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import sda.beermap.config.RootConfig;
import sda.beermap.config.WebConfig;
import sda.beermap.service.LoginService;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {RootConfig.class, WebConfig.class})
//@WebAppConfiguration
public class LoginControllerTest {
	
	private final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	
	@Before
    public void setup() {
		resolver.setPrefix("/WEB-INF/view/");
		resolver.setSuffix(".jsp");	
    }

    @Test
    public void shouldShowLoginForm() throws Exception {
        LoginController controller = new LoginController();

        // Ustawiamy atrapę MockMvc
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(resolver)
                .build();
        
        // Wywołujemy żądanie GET o zasób "/" i oczekujemy zwrócenia widoku login
        mockMvc.perform(get("/login")).andExpect(view().name("login"));
    }
    
//    @Test
//    public void shouldProcessLogin() throws Exception {
//    	LoginService loginService = mock(LoginService.class); // Tworzymy atrapę serwisu
//    }

}
